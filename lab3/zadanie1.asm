.686 
.model flat 
extern  __write : PROC 
extern _ExitProcess@4 : PROC

public _main 
.data 
znaki	db	12 dup (?)
.code

_wyswietl_EAX PROC
	pusha
	mov	esi,10
	mov ebx,10	;dzielnik
konwersja:
	mov	edx,0
	div	ebx
	add	dl, 30h
	mov	znaki[esi],dl
	dec esi
	cmp	eax,0
	jne konwersja
wypeln:
	or	esi,esi
	jz	wyswietl
	mov	byte ptr znaki[esi],20h
	dec esi
	jmp wypeln
	wyswietl:
	mov	byte ptr znaki[0],0Ah
	mov byte ptr znaki[11],0Ah
	push dword ptr 12
	push dword ptr OFFSET znaki
	push dword ptr 1
	call __write
	add esp,12
	popa
	ret
_wyswietl_EAX ENDP

_main PROC
	mov eax,1
poczatek:
	cmp eax,51
	je koniec
	call	_wyswietl_EAX
	inc eax
	jmp poczatek
koniec:
	push 0
	call _ExitProcess@4 ; zako?czenie programu
_main ENDP
END 