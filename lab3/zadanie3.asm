.686 
.model flat 
extern __read : PROC
extern __write : PROC
extern _ExitProcess@4 : PROC

public _main 
.data 
obszar	db	12 dup (?)
dziesiec	dd	10

.code

_wyswietl_EAX PROC
	pusha
	mov	esi,10
	mov ebx,10	;dzielnik
konwersja:
	mov	edx,0
	div	ebx
	add	dl, 30h
	mov	obszar[esi],dl
	dec esi
	cmp	eax,0
	jne konwersja
wypeln:
	or	esi,esi
	jz	wyswietl
	mov	byte ptr obszar[esi],20h
	dec esi
	jmp wypeln
	wyswietl:
	mov	byte ptr obszar[0],0Ah
	mov byte ptr obszar[11],0Ah
	push dword ptr 12
	push dword ptr OFFSET obszar
	push dword ptr 1
	call __write
	add esp,12
	popa
	ret
_wyswietl_EAX ENDP

_wczytaj_EAX PROC	
	push	ebp
	mov		ebp,esp
	push	ebx
	push	esi
	push	edi
	;

	push	dword ptr 12
	push	dword PTR offset obszar
	push	dword ptr 0
	call	__read
	add		esp,12
	mov		eax,0
	mov		ebx,OFFSET obszar
pobieraj_znaki:
	mov		cl,[ebx]
	inc		ebx
	cmp		cl,10
	je		byl_enter
	sub		cl, 30h
	movzx	ecx,cl
	mul		dword ptr dziesiec
	add		eax,ecx
	jmp		pobieraj_znaki
byl_enter:

	;
	pop		edi
	pop		esi
	pop		ebx
	pop		ebp
	ret
_wczytaj_EAX ENDP

_main PROC
	call _wczytaj_EAX
	mul	eax
	mov	dword ptr obszar, eax
	call _wyswietl_EAX	
	push 0
	call _ExitProcess@4 ; zako?czenie programu
_main ENDP
END 