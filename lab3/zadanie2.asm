.686 
.model flat 
extern __read : PROC
extern _ExitProcess@4 : PROC

public _main 
.data 
obszar	db	12 dup (?)
dziesiec	dd	10

.code
_wczytaj_EAX PROC	
	push	ebp
	mov		ebp,esp
	push	ebx
	push	esi
	push	edi
	;

	push	dword ptr 12
	push	dword PTR offset obszar
	push	dword ptr 0
	call	__read
	add		esp,12
	mov		eax,0
	mov		ebx,OFFSET obszar
pobieraj_znaki:
	mov		cl,[ebx]
	inc		ebx
	cmp		cl,10
	je		byl_enter
	sub		cl, 30h
	movzx	ecx,cl
	mul		dword ptr dziesiec
	add		eax,ecx
	jmp		pobieraj_znaki
byl_enter:

	;
	pop		edi
	pop		esi
	pop		ebx
	pop		ebp
	ret
_wczytaj_EAX ENDP

_main PROC
	call _wczytaj_EAX
	nop
	push 0
	call _ExitProcess@4 ; zako?czenie programu
_main ENDP
END 