.686 
.model flat 
extern __read : PROC
extern __write : PROC
extern _ExitProcess@4 : PROC

public _main 
.data 
obszar		db	12 dup (?)
dziesiec	dd	10
wynik		dd	6 dup (?)
dzielnik	dd	?
blad		db '-1'
.code

_wyswietl_EAX PROC
	pusha
	mov	esi,10
	mov ebx,10	;dzielnik
konwersja:
	mov	edx,0
	div	ebx
	add	edx, 30h
	mov	obszar[esi],dl
	dec esi
	cmp	eax,0
	jne konwersja
wypeln:
	or	esi,esi
	jz	wyswietl
	mov	byte ptr obszar[esi],20h
	dec esi
	jmp wypeln
	wyswietl:
	mov	byte ptr obszar[0],0Ah
	mov byte ptr obszar[11],0Ah
	push dword ptr 12
	push dword ptr OFFSET obszar
	push dword ptr 1
	call __write
	add esp,12
	popa
	ret
_wyswietl_EAX ENDP

_wczytaj_EAX PROC	
	push	ebp
	mov		ebp,esp
	push	ebx
	push	esi
	push	edi
	;

	push	dword ptr 12
	push	dword PTR offset obszar
	push	dword ptr 0
	call	__read
	add		esp,12
	mov		eax,0
	mov		ebx,OFFSET obszar
pobieraj_znaki:
	mov		cl,[ebx]
	inc		ebx
	cmp		cl,10
	je		byl_enter
	sub		cl, 30h
	movzx	ecx,cl
	mul		dword ptr dziesiec
	add		eax,ecx
	jmp		pobieraj_znaki
byl_enter:

	;
	pop		edi
	pop		esi
	pop		ebx
	pop		ebp
	ret
_wczytaj_EAX ENDP

_znajdz_podzielna PROC
	push	ebp
	mov		ebp,esp
	push	esi
	push	edi
	push	ebx
	;
	mov		ebx,[ebp+8]		;wskaznik na pierwszy element tablicy liczb
	mov		edi,[ebp+12]	;dzielnik
	mov		esi,0
	mov		cl,0
	mov		ch,0
ptl:
	cmp		cl,6
	je		koniec
	mov		eax,dword ptr [ebx][esi]
	add		esi,4
	mov		edx,0
	div		edi
	cmp		edx,0
	jne		dalej
	;mamy podzielna przez dzielnik
	push ecx
	sub	esi,4
	mov	eax,dword ptr [ebx][esi]
	add	esi,4
	call _wyswietl_EAX
	pop ecx
	inc ch
dalej:
	inc cl
	jmp	ptl
koniec:
	cmp	ch,0
	jne	kkoniec
	push 2
	push offset blad
	push 1
	call __write
	add esp,12
kkoniec:
	;
	pop		ebx
	pop		edi
	pop		esi
	pop		ebp
	ret		8
_znajdz_podzielna ENDP
_main PROC
	mov ecx,6
	mov ebx,0
ptl:
	push ecx
	call _wczytaj_EAX
	pop	ecx
	mov wynik[ebx],eax
	add ebx,4
	loop ptl
	call _wczytaj_EAX	
	push	eax
	push	OFFSET wynik
	call _znajdz_podzielna	
	add	esp,8
	push 0
	call _ExitProcess@4 ; zako?czenie programu
_main ENDP
END 