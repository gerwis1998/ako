.686 
.model flat 
extern __read : PROC
extern __write : PROC
extern _ExitProcess@4 : PROC

public _main 
.data 
obszar		db	12 dup (?)
dziesiec	dd	10
dekoder		db '0123456789ABCDEF'
.code

_wyswietl_EAX PROC
	pusha
	mov	esi,10
	mov ebx,10	;dzielnik
konwersja:
	mov	edx,0
	div	ebx
	add	dl, 30h
	mov	obszar[esi],dl
	dec esi
	cmp	eax,0
	jne konwersja
wypeln:
	or	esi,esi
	jz	wyswietl
	mov	byte ptr obszar[esi],20h
	dec esi
	jmp wypeln
	wyswietl:
	mov	byte ptr obszar[0],0Ah
	mov byte ptr obszar[11],0Ah
	push dword ptr 12
	push dword ptr OFFSET obszar
	push dword ptr 1
	call __write
	add esp,12
	popa
	ret
_wyswietl_EAX ENDP

_wczytaj_EAX PROC	
	push	ebp
	mov		ebp,esp
	push	ebx
	push	esi
	push	edi
	;

	push	dword ptr 12
	push	dword PTR offset obszar
	push	dword ptr 0
	call	__read
	add		esp,12
	mov		eax,0
	mov		ebx,OFFSET obszar
pobieraj_znaki:
	mov		cl,[ebx]
	inc		ebx
	cmp		cl,10
	je		byl_enter
	sub		cl, 30h
	movzx	ecx,cl
	mul		dword ptr dziesiec
	add		eax,ecx
	jmp		pobieraj_znaki
byl_enter:

	;
	pop		edi
	pop		esi
	pop		ebx
	pop		ebp
	ret
_wczytaj_EAX ENDP

_wyswietl_EAX_hex PROC
	pusha
	sub		esp,12
	mov		edi,esp
	mov		ecx,8
	mov		esi,1
ptl3hex:
	rol		eax,4
	mov		ebx,eax
	and		ebx,0000000FH
	jnz		nie_zero
	;jesli zero
	mov		dl, 20h
	jmp		dalej
nie_zero:
	mov		dl,dekoder[ebx]
dalej:
	mov		[edi][esi],dl
	inc		esi
	loop	ptl3hex
	mov		byte ptr [edi][0],10
	mov		byte ptr [edi][9],10
	push	10
	push	edi
	push	1
	call	__write
	add		esp,12
	add		esp,12
	popa
	ret
_wyswietl_EAX_hex ENDP

_wczytaj_do_EAX_hex PROC
; wczytywanie liczby szesnastkowej z klawiatury � liczba po 
; konwersji na posta� binarn� zostaje wpisana do rejestru EAX
; po wprowadzeniu ostatniej cyfry nale�y nacisn�� klawisz
; Enter
	 push ebx
	 push ecx
	 push edx
	 push esi
	 push edi
	 push ebp
	; rezerwacja 12 bajt�w na stosie przeznaczonych na tymczasowe
	; przechowanie cyfr szesnastkowych wy�wietlanej liczby
	 sub esp, 12 ; rezerwacja poprzez zmniejszenie ESP
	 mov esi, esp ; adres zarezerwowanego obszaru pami�ci
	 push dword PTR 10 ; max ilo�� znak�w wczytyw. liczby
	 push esi ; adres obszaru pami�ci
	 push dword PTR 0; numer urz�dzenia (0 dla klawiatury)
	 call __read ; odczytywanie znak�w z klawiatury
	 ; (dwa znaki podkre�lenia przed read)
	 add esp, 12 ; usuni�cie parametr�w ze stosu
	 mov eax, 0 ; dotychczas uzyskany wynik
pocz_konw:
	 mov dl, [esi] ; pobranie kolejnego bajtu
	 inc esi ; inkrementacja indeksu
	 cmp dl, 10 ; sprawdzenie czy naci�ni�to Enter
	 je gotowe ; skok do ko�ca podprogramu
	; sprawdzenie czy wprowadzony znak jest cyfr� 0, 1, 2 , ..., 9
	 cmp dl, '0'
	 jb pocz_konw ; inny znak jest ignorowany
	 cmp dl, '9'
	 ja sprawdzaj_dalej
	 sub dl, '0' ; zamiana kodu ASCII na warto�� cyfry
dopisz:
	 shl eax, 4 ; przesuni�cie logiczne w lewo o 4 bity
	 or al, dl ; dopisanie utworzonego kodu 4-bitowego
	 ; na 4 ostatnie bity rejestru EAX
	 jmp pocz_konw ; skok na pocz�tek p�tli konwersji
	; sprawdzenie czy wprowadzony znak jest cyfr� A, B, ..., F
sprawdzaj_dalej:
	cmp dl, 'A'
	jb pocz_konw ; inny znak jest ignorowany
	cmp dl, 'F'
	ja sprawdzaj_dalej2
	sub dl, 'A' - 10 ; wyznaczenie kodu binarnego
	jmp dopisz 
; sprawdzenie czy wprowadzony znak jest cyfr� a, b, ..., f
sprawdzaj_dalej2:
	cmp dl, 'a'
	jb pocz_konw ; inny znak jest ignorowany
	cmp dl, 'f'
	ja pocz_konw ; inny znak jest ignorowany
	sub dl, 'a' - 10
	jmp dopisz
gotowe:
	; zwolnienie zarezerwowanego obszaru pami�ci
	add esp, 12
	pop ebp
	pop edi
	pop esi
	pop edx
	pop ecx
	pop ebx
	ret
_wczytaj_do_EAX_hex ENDP

_main PROC
	call _wczytaj_do_EAX_hex
	call _wyswietl_EAX
	push 0
	call _ExitProcess@4 ; zako?czenie programu
_main ENDP
END 