public suma_siedmiu_liczb
.code 
suma_siedmiu_liczb   PROC 
     push	rbx       ; przechowanie rejestr�w 
	 mov	rbx, rsp
     push	rsi 
     push	rdi
	 push	rbp
		;dodanie 4 pierwszych liczb
		mov rax,rcx
		add rax,rdx
		add rax,r8
		add rax,r9
		;dodawanie 3 kolejnych liczb ze stosu
		add rbx, 48
		mov rdx, 0
		mov rcx, 3
ptl:	
		add rax,[rbx][rdx]
		add rdx,8
		loop ptl
	 pop	rbp
	 pop	rdi
     pop	rsi 
     pop	rbx 
     ret 
suma_siedmiu_liczb   ENDP 
END 