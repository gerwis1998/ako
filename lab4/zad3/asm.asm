.686
.model flat 
public _odejmij_jeden
.code
_odejmij_jeden PROC
push   ebp
mov    ebp,esp
push   esi
push   edi
push   ebx
;
	mov ebx,[ebp+8] ;bierzemy do ebx wskaznik na wskaznik
	mov eax,[ebx] ;bierzemy do eax wskaznik na liczb�
	mov ecx,[eax]	;zapisujemy do ecx liczbe
	dec ecx
	mov [eax],ecx	;zamieniamy liczbe
;
pop    ebx
pop    edi
pop    esi
pop    ebp
ret
_odejmij_jeden ENDP
END 