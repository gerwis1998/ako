.686
.model flat 
public _szukaj4_max
.code
_szukaj4_max PROC
push   ebp
mov    ebp,esp
push   esi
push   edi
push   ebx
;
	mov	eax, [ebp+8] ;liczba a, poczatkowy max
	mov ecx, 3
	mov edx, 12	;index
ptl:
	mov ebx, [ebp][edx]
	cmp	ebx, eax
	jl	dalej
	;jesli ebx jest wieksze od max to zamien
	mov	eax, ebx
dalej:
	add edx,4
	loop ptl
zakoncz:
;
pop    ebx
pop    edi
pop    esi
pop    ebp
ret
_szukaj4_max ENDP
END 