.686
.model flat
.XMM
extern _malloc : PROC
.code
_szyfruj PROC
push   ebp
mov    ebp,esp
push   esi
push   edi
push   ebx
;
	mov esi,[ebp+8]		;adres tekstu
	mov edi,52525252h
ptl:
	movzx ebx, byte ptr [esi]
	cmp bl,0
	je koniec
	mov eax,edi
	xor [esi],al
	inc esi
	mov eax,0
	bt edi,30
	rol dl,1
	bt edi,31
	rol dh,1
	xor dl,dh
	mov dh,0			;w edx jest ta suam modulo dwa
	shl edi,1
	add edi,edx
	jmp ptl
koniec:
;
pop    ebx
pop    edi
pop    esi
pop    ebp
	ret
_szyfruj ENDP

_kwadrat PROC
push   ebp
mov    ebp,esp
push   esi
push   edi
push   ebx
;
	mov esi,[ebp+8]
	cmp esi,0
	je zero
	cmp esi,1
	je jeden
wieksze:
	mov ebx,esi
	lea ebx,[4*ebx - 4]
	mov eax,esi
	sub eax,2
	push eax
	call _kwadrat
	add esp,4
	add eax,ebx
	jmp koniec
jeden:
	mov eax,1
	jmp koniec
zero:
	mov eax,0
	jmp koniec
koniec:
;
pop    ebx
pop    edi
pop    esi
pop    ebp
ret
_kwadrat ENDP

_zad8 PROC
push   ebp
mov    ebp,esp
push   esi
push   edi
push   ebx
;
	mov ebx,[ebp+8]
	mov edx,0
	mov dl,10000000b
	and bl,dl
	cmp ebx,0
	je zero
	stc
	jmp koniec
zero:
	clc
	jmp koniec
koniec:
;
pop    ebx
pop    edi
pop    esi
pop    ebp
ret
_zad8 ENDP

_zad9 PROC
push   ebp
mov    ebp,esp
push   esi
push   edi
push   ebx
;
	mov edx,[ebp+8]
	bt edx,7
	jnc zero
jeden:
	shr edx,7
	add edx,1
	shl edx,7
	jmp koniec
zero:
	shr edx,7
	shl edx,7
koniec:
;
pop    ebx
pop    edi
pop    esi
pop    ebp
ret
_zad9 ENDP

_zad10 PROC
push   ebp
mov    ebp,esp
push   esi
push   edi
push   ebx
;
	mov esi,[ebp+8]
	mov edi,[ebp+12]
	rcl edi,1
	jc mniejsza
	mov ecx,31
ptl:
	mov eax,0
	rcl esi,1
	rcl al,1
	rcl edi,1
	rcl ah,1
	cmp al,ah
	ja wieksza
	jb mniejsza
	loop ptl
	rcl esi,1
	jc wieksza
mniejsza:
	clc
	jmp koniec
wieksza:
	stc
koniec:
;
pop    ebx
pop    edi
pop    esi
pop    ebp
ret
_zad10 ENDP

_zad14 PROC
push   ebp
mov    ebp,esp
push   esi
push   edi
push   ebx
;
	mov esi,[ebp+8]	;w esi liczba we floatcie
	mov eax,8
	push eax
	call _malloc
	add esp,4
	mov edi,eax		;w edi adres wynikowej tablicy
	mov eax,esi
	mov ebx,0
	shr eax,23		;zostawiamy tylko 9 pierwszych bit�w, w al jest 8 bit�w wyk�adnika
	movzx ebx,al	;w ebx jest 8 bit�w wyk�adnika
	sub ebx,127
	add ebx,1023	;w bx jest 11 bit�w wyk�adnika + 5 niepotrzebnych
	shl ebx,16+5	;najstarsze bity ebx to 11 bit�w wyk�adnika
	mov eax,esi
	bt eax,31
	rcr	ebx,1		;wpisanie bitu znaku na koniec ebx, 12 bit�w wyk�adnika i znaku gotowe, potrzebujemy 20 bit�w jeszcze
	shl eax,9		;usuwamy wyk�adnik i bit znaku
	shr eax,9+3		;usuwamy 3 ostatnie bity kt�re si� nie mieszcz�
	or ebx,eax		;wstawiamy do ebx 20 bit�w
	mov [edi+4],ebx	;wstawiamy starsz� cz�� liczby na koniec bo little endian
	mov eax,esi
	shl eax,29		;zostawiamy tylko najm�odsze 3 bity na najstarszych wynikowych
	mov [edi],eax
	mov eax,edi
;
pop    ebx
pop    edi
pop    esi
pop    ebp
ret
_zad14 ENDP

_float2double PROC
push   ebp
mov    ebp,esp
push   esi
push   edi
push   ebx
;
	movss xmm2,dword ptr[ebp+8]
	mov eax,4
	push eax
	call _malloc
	add esp,4
	cvtss2sd xmm1,xmm2
	movsd qword ptr[eax],xmm1
;
pop    ebx
pop    edi
pop    esi
pop    ebp
ret
_float2double ENDP

_pole_kola PROC
push   ebp
mov    ebp,esp
push   esi
push   edi
push   ebx
;
	mov esi,[ebp+8]
	finit

	fldpi
	fld dword ptr [esi]
	fmul st(0),st(0)		;st(0) = r^2 st(1)  = pi
	fmul st(0),st(1)
	fxch st(1)
	fstp st(0)
	fstp dword ptr [esi]
;
pop    ebx
pop    edi
pop    esi
pop    ebp
ret
_pole_kola ENDP

_plus_jeden PROC
push   ebp
mov    ebp,esp
push   esi
push   edi
push   ebx
;
	mov eax,[ebp+8]
	mov edx,[ebp+12]
	;wpisanie 1 na pozycj� 2^0 mantysy edi:esi
	mov esi,0
	mov edi,00100000h
	mov ebx,edx
	shr ebx,20
	sub ebx,1023
	and edx,000FFFFFh
	or	edx,00100000h
	;w EDI:ESI ma by� liczba wynikowa, w ebx mamy wyk�adnik odpolaryzowany, w edx mamy liczb� nieprzesuni�t�
	;z dodatkow� jawn� 1 przy mantysie
	mov ecx,ebx
	shr edi,cl
	add edx,edi
	bt	edx,21
	jnc bez_korekcji
	btr edx,21		;usuwamy jawn� jedynk� dodatkow� je�li przekroczy�o
	ror edx,1
	rcr eax,1
	add ebx,1
bez_korekcji:
	btr edx,20		;usuwamy jawn� jedynk�
	add ebx,1023
	shl ebx,20
	or	edx,ebx
	finit
	push edx
	push eax
	fld qword ptr [esp]
	add esp,8
;
pop    ebx
pop    edi
pop    esi
pop    ebp
ret
_plus_jeden ENDP

_avg_wd PROC
push   ebp
mov    ebp,esp
push   esi
push   edi
push   ebx
;
	mov ecx,[ebp+8]		;liczba element�w tablicy
	mov esi,[ebp+12]	;tablica liczb
	mov edi,[ebp+16]	;tablica wag
	finit
	fldz				;suma wag
	fldz				;suma wag*liczb
	;st(0) = suma wag*liczb, st(1) = suma wag
	mov edx,0
ptl:
	mov eax,[edi+edx]
	push eax
	fld dword ptr [esp]
	add esp,4
	;st(0) = waga, st(1) = suma wag*liczb poprzednich, st(2) = suma wag poprzednich
	fxch st(2)
	fadd st(0),st(2)
	fxch st(2)
	;st(0) = waga, st(1) = suma wag*liczb poprzednich, st(2) = suma wag
	mov eax,[esi+edx]
	push eax
	fld dword ptr [esp]
	add esp,4
	;st(0) = liczba, st(1) =waga, st(2) = suma wag*liczb poprzednich, st(3) = suma wag poprzednich
	fmul st(0),st(1)	;st(0) = liczba*waga, st(1) = waga...
	fxch st(1)
	fstp st(0)
	fxch st(1)			;-> st(0) = suma wag*liczb poprzednich, st(1) = liczba*waga...
	fadd st(0),st(1)
	fxch st(1)
	fstp st(0)			;-> st(0) = suma wag*liczb, st(1) = suma wag
	add edx,4
	loop ptl
	fdiv st(0),st(1)
	fxch st(1)

	fstp st(0)
;
pop    ebx
pop    edi
pop    esi
pop    ebp
ret
_avg_wd ENDP

_nwd PROC
push   ebp
mov    ebp,esp
push   esi
push   edi
push   ebx
;
	mov esi,[ebp+8]
	mov edi,[ebp+12]
	cmp esi,edi
	jne nie_rowne
	mov eax,esi
	jmp koniec
nie_rowne:
	cmp esi,edi
	jb	mniejsze
	mov eax,esi
	mov ebx,edi
	sub eax,ebx
	push ebx
	push eax
	call _nwd
	add esp,8
	jmp koniec
	;
mniejsze:
	mov eax,esi	;a
	mov ebx,edi	;b
	sub ebx,eax	;b-a
	push ebx
	push eax
	call _nwd
	add esp,8
koniec:
;
pop    ebx
pop    edi
pop    esi
pop    ebp
ret
_nwd ENDP
 END

