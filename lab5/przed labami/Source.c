#include <stdio.h>
#include <stdlib.h>
float sr_kwadrat(float*, int);
float suma_szeregu_kw(int);
int main()
{
	int n;
	printf_s("Podaj ilosc liczb: ");
	scanf_s("%i", &n);
	float * tab = (float*)malloc(n * sizeof(float));
	printf_s("Podawaj liczy: \n");
	for(int i=0; i<n; i++)
	{
		scanf_s("%f", &tab[i]);
	}
	float kw = sr_kwadrat(tab, n);
	float ss = suma_szeregu_kw(n);
	printf_s("Srednia kwadratowa to: %f\n", kw);
	printf_s("Suma szeregu kwadratowego %i liczb to: %f", n, ss);
	return 0;
}