.686
.XMM  
.model flat
public _sr_kwadrat
public _suma_szeregu_kw
.code
_sr_kwadrat PROC
push   ebp
mov    ebp,esp
push   esi
push   edi
push   ebx
;
	mov esi,[ebp+8]			;wskaznik na tablice
	mov edi,[ebp+12]		;ilosc elementow
	mov ecx,0
	mov edx,0
	finit
	fldz					;st(0) = suma
ptl:
	cmp ecx,edi
	je koniec
	mov eax,[esi+edx]
	push eax
	fld dword ptr[esp]
	add esp,4				;st(0) = a[n], st(1) = suma poprzednich
	fmul st(0),st(0)		;st(0) = a[n]^2, st(1) = suma poprzednich
	fxch st(1)				;st(0) = suma poprzednich, st(1) = a[n]^2
	fadd st(0), st(1)				;st(0) = suma, st(1) = a[n]^2
	fxch st(1)				;st(0) = a[n]^2, st(1) = suma
	fstp st(0)				;st(0) = suma
	add edx,4
	add ecx,1
	jmp ptl
koniec:
	;st(0) = suma kwadrat�w
	push edi
	fild dword ptr [esp]
	add esp,4
	;st(0) = n, st(1) = suma
	fxch st(1)
	fdiv st(0),st(1)
	;st(0) = suma/n, st(1) = n
	fxch st(1)
	fstp st(0)
	;st(0) = suma/n
	fsqrt
;
pop    ebx
pop    edi
pop    esi
pop    ebp
ret
_sr_kwadrat ENDP

_suma_szeregu_kw PROC
push   ebp
mov    ebp,esp
push   esi
push   edi
push   ebx
;
	mov esi,[ebp+8]
	mov ecx,0
	finit
	fldz
ptl:
	cmp ecx,esi
	je koniec
	add ecx,1
	push ecx
	fild dword ptr [esp]
	add esp,4
	;st(0) = n, st(1) = suma poprzednich
	fmul st(0),st(0)
	;st(0) = n^2, st(1) = suma poprzednich
	fxch
	;st(0) = suma poprzednich, st(1) = n^2
	fadd st(0),st(1)
	fxch
	fstp st(0)
	;st(0) = suma
	jmp ptl
koniec:
;
pop    ebx
pop    edi
pop    esi
pop    ebp
ret
_suma_szeregu_kw ENDP
END