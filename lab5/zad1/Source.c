#include <stdio.h>
#include <stdlib.h>
float srednia_harm(float  *  tablica, unsigned  int  n);
int main()
{
	unsigned int n;
	printf_s("Podaj ilosc liczb: ");
	scanf_s("%u", &n);
	printf_s("Podaj liczby: \n");
	float * tab = (float*)malloc(n * sizeof(float));
	for(unsigned int i=0; i<n; i++){
		scanf_s("%f", &tab[i]);
	}
	float w = srednia_harm(tab, n);
	printf_s("Srednia harmoniczna tych liczb to: %f", w);
	return 0;
}