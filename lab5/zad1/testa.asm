.686
public _srednia_harm
.model flat
.data
tst dd ?
.code 
_srednia_harm PROC
push   ebp
mov    ebp,esp
	mov ecx,[ebp+12]
	mov eax,ecx
	lea eax,[4*eax]	;4 bajty * liczba zmiennych w tablicy
	sub esp,eax		;przygotowanie miejsca na zmienne 
push   esi
push   edi
push   ebx
finit
;
	mov ebx,[ebp+8] ;w ebx jest adres tablicy liczb
	mov edx,0
ptl_a:
	cmp ecx,0
	je koniec_a
	mov eax,[ebx+edx]
	add edx,4
	sub ecx,1
	push eax
	fld dword ptr [esp]
	add esp,4
	fld1
	fdiv st(0),st(1)
	mov eax,ebp
	sub eax,edx
	fstp dword ptr [eax]	;zapis do pamieci wyniku dzielenie przez 1
	fstp st(0)
	jmp ptl_a
koniec_a:
	mov ecx,[ebp+12]
	mov edx,4
	fldz	;zaladowanie 0 na stos
ptl_b:
	cmp ecx,0
	je koniec_b
	sub ecx,1
	mov eax,ebp
	sub eax,edx
	mov eax,[eax]
	add edx,4
	push eax
	fld dword ptr [esp]
	add esp,4	;wrzucenie liczby na stos 
	fadd st(0),st(1)
	fxch st(1)
	fstp st(0)	;na stosie zostaje akutalna suma
	jmp ptl_b
koniec_b:
	;na stosie mamy sume 1/a1 itd
	mov ecx,[ebp+12]
	fldz	;wrzucamy 0 na stos
ptl_c:
	cmp ecx,0
	je koniec_c
	sub ecx,1
	fld1
	fadd st(0),st(1)
	fxch st(1)
	fstp st(0)
	jmp ptl_c
koniec_c:
	fdiv st(0),st(1)
	fstp tst
	fld dword ptr tst
pop    ebx
pop    edi
pop    esi
	mov ecx,[ebp+12]
	mov eax,ecx
	lea eax,[4*eax]	;4 bajty * liczba zmiennych w tablicy
	add esp,eax		;przygotowanie miejsca na zmienne 
pop    ebp
ret
_srednia_harm ENDP
END 