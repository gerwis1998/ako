.686
.XMM
public _int2float
.model flat
.code 
_int2float PROC
push   ebp
mov    ebp,esp
push   esi
push   edi
push   ebx
;
	mov   esi, [ebp+8]    ; adres pierwszej tablicy
	mov   edi, [ebp+12]   ; adres drugiej tablicy
	cvtpi2ps xmm5, qword ptr [esi]
	movups [edi], xmm5
;
pop    ebx
pop    edi
pop    esi
pop    ebp
ret
_int2float ENDP
END 