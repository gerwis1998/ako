.686
.XMM
public _pm_jeden
.model flat
.data
jeden	real4	1.0,1.0,1.0,1.0
.code 
_pm_jeden PROC
push   ebp
mov    ebp,esp
push   esi
push   edi
push   ebx
;
	mov   esi, [ebp+8]    ; adres pierwszej tablicy
	movups xmm5,[esi]
	mov	edi,OFFSET jeden
	addsubps xmm5,[edi]
	movups [esi],xmm5
;
pop    ebx
pop    edi
pop    esi
pop    ebp
ret
_pm_jeden ENDP
END 