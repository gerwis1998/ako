.686
.XMM
public _dodaj
.model flat
.code 
_dodaj PROC
push   ebp
mov    ebp,esp
push   esi
push   edi
push   ebx
;
	mov   esi, [ebp+8]    ; adres pierwszej tablicy
	mov   edi, [ebp+12]   ; adres drugiej tablicy
	mov   ebx, [ebp+16]   ; adres tablicy wynikowej
	movups	xmm5,[esi]
	movups	xmm6,[edi]
	paddsb	xmm5,xmm6
	movups [ebx],xmm5
;
pop    ebx
pop    edi
pop    esi
pop    ebp
ret
_dodaj ENDP
END 