.686
public _nowy_exp
.model flat
.code 
_nowy_exp PROC
push   ebp
mov    ebp,esp
push   esi
push   edi
push   ebx
	finit
	mov ebx,[ebp+8]	;liczba x w ebx
	mov ecx,0
	fld1			;suma wszystkich na stos
	fld1			;na stosie st(0)=1 - pierwszy element ci�gu, st(1)=0 - suma
ptl_a:
	cmp ecx,19
	je koniec_a
	add ecx,1
	;za�adowanie kolejnej liczby do pomnozenia na stos
	mov eax,ecx
	push eax
	fild dword ptr [esp]	;st(0) = aktualna liczba do pomnozenia, st(1) = poprzedni element ci�gu dzielenia, st(2) = suma poprzednich
	add esp,4
	fmul st(1),st(0)		;pomnozenie aktualnej liczby do pomnozenia przez iloczyn poprzednich i zapisanie w st(1)
	fstp st(0)				;usuni�cie st(0) ze stosu
	push ebx
	fld dword ptr [esp]
	add esp,4				;wrzucenie x na stos -> st(0)=x, st(1) = iloczyn, st(2) = suma poprzednich
	push ecx
	sub ecx,1
ptl_b:						;liczenie x^n-1
	cmp ecx,0
	je koniec_b
	push ebx
	fld dword ptr [esp]
	add esp,4
	fmul st(0),st(1)
	fxch st(1)
	fstp st(0)
	sub ecx,1
	jmp ptl_b
koniec_b:
	pop ecx
	;st(0) = x^(n-1), st(1) = iloczyn, st(2) = suma poprzednich
	fdiv st(0),st(1)	;->st(0) = x^(n-1)/iloczyn, st(1) - iloczyn, st(2) - suma poprzednich
	fxch st(2)			;->st(0) = suma poprzednich, st(1) = iloczyn, st(2) = x^(n-1)/iloczyn
	fadd st(0),st(2)
	fxch st(2)			;-> st(0) = x^(n-1)/iloczyn, st(1) = iloczyn, st(2) = suma 
	fstp st(0)			;-> st(0) = iloczyn, st(1) = suma
	jmp ptl_a
koniec_a:
	fxch st(1)	;-> st(0) = suma, st(1) = iloczyn
pop    ebx
pop    edi
pop    esi
pop    ebp
ret
_nowy_exp ENDP
END 