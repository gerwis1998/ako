.686
.model flat
public _foo
.code
_foo PROC
push   ebp
mov    ebp,esp
push   esi
push   edi
push   ebx
;
	mov esi, [ebp+8]
	add esi,1
	mov ecx,1
	finit
	fldz			;st(0) = suma
ptl:
	cmp ecx,esi
	je koniec
	push ecx
	fild dword ptr [esp]
	add esp,4
	;st(0) = aktualna liczba, st(1) = suma poprzednich
	fmul st(0),st(0)
	;st(0) = n^2, st(1) = suma poprzednich
	fld1
	;st(0) = 1, st(1) = n^2, st(2) = suma poprzednich
	fdiv st(0),st(1)
	;st(0) = 1/n^2, st(1) = n^2, st(2) = suma poprzednich
	fxch st(1)
	;st(0) = n^2, st(1) = 1/n^2, st(2) = suma poprzednich
	fstp st(0)
	;st(0) = 1/n^2, st(1) = suma poprzednich
	fadd st(1),st(0)
	;st(0) = 1/n^2, st(1) = suma aktualna
	fstp st(0)
	;st(0) = suma aktualna
	add ecx,1
	jmp ptl
koniec:
;
pop    ebx
pop    edi
pop    esi
pop    ebp
ret
_foo ENDP
END