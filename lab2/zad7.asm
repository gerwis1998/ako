; wczytywanie i wy�wietlanie tekstu wielkimi literami
; (inne znaki si� nie zmieniaj�)
.686
.model flat
extern _ExitProcess@4 : PROC
extern _MessageBoxA@16 : PROC
extern _MessageBoxW@16 : PROC
extern __write : PROC
extern __read  : PROC; (dwa znaki podkre�lenia)
public  _main
.data
tekst_pocz		db	10, 'Prosz', 169,' napisa', 134,' jaki', 152,' tekst '
				db	'i nacisn', 165, 134,' Enter', 10

koniec_t		db	?

magazyn			db	80 dup (?)

nowa_linia		db	10

liczba_znakow	dd	?

latin			db	165, 134, 169, 136, 228, 162, 152, 171, 190
				db	164, 143, 168, 157, 227, 224, 151, 141, 189

windows			db	185, 230, 234, 179, 241, 243, 156, 159, 191
				db	165, 198, 202, 163, 209, 211, 140, 143, 175

utf				dw	105H, 107H, 119H, 142H, 144H, 0F3H, 15BH, 17AH, 17CH
				dw	104H, 106H, 118H, 141H, 143H, 0D3H, 15AH, 179H, 17BH

magazyn2		dw	80 dup (?)

nowa_linia2		dw	10

.code

_main PROC

; wy�wietlenie tekstu informacyjnego
; liczba znak�w tekstu
mov     ecx, (OFFSET koniec_t) -	(OFFSET tekst_pocz)
push    ecx
push    OFFSET tekst_pocz  ; adres tekstu
push	1 ; nr urz�dzenia (tu: ekran -nr 1)
call    __write		; wy�wietlenie tekstu pocz�tkowego
add     esp, 12		; usuniecie parametr�w ze stosu

; czytanie wiersza z klawiatury
push    80 ; maksymalna liczba znak�w
push    OFFSET magazyn
push    0  ; nr urz�dzenia (tu: klawiatura -nr 0)
call    __read ; czytanie znak�w z klawiatury
add     esp, 12	; usuniecie parametr�w ze stosu

; kody ASCII napisanego tekstu zosta�y wprowadzone do obszaru 'magazyn'
; funkcja read wpisuje do rejestru EAX liczb� wprowadzonych znak�w
mov     liczba_znakow, eax

; rejestr ECX pe�ni rol� licznika obieg�w p�tli
mov     ecx, eax
mov     ebx, 0	; indeks pocz�tkowy
mov		eax, 0  ; indeks poczatkowy utf16
ptl:
mov     dl, magazyn[ebx] ; pobranie kolejnego znaku
pusha	
mov		ecx, 0
mov		edi, 0
petla2:	;wewnetrzna petla
cmp		dl, latin[ecx]
jne		nie_ta
mov		dl, windows[ecx]
mov		magazyn[ebx], dl
mov		dx, utf[edi]
mov		magazyn2[eax], dx
jmp		dalej
nie_ta:
inc		ecx
add		edi,2
cmp		ecx, 18
jne		petla2
; odes�anie znaku do pami�ci
mov     magazyn[ebx], dl
mov		dh,0
mov		magazyn2[eax], dx
dalej:
popa
add		eax, 2
inc     ebx		; inkrementacja indeksu
loop	ptl     ; sterowanie p�tl�

; wy�wietlenie przekszta�conego tekstu
push	0
push	OFFSET magazyn
push	OFFSET magazyn
push	0
call	_MessageBoxA@16
push	0
push	OFFSET magazyn2
push	OFFSET magazyn2
push	0
call	_MessageBoxW@16
push	0
call    _ExitProcess@4   ; zako�czenie programu
_main ENDP
END