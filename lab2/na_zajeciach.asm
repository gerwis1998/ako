; wczytywanie i wy�wietlanie tekstu wielkimi literami
; (inne znaki si� nie zmieniaj�)
.686
.model flat
extern _ExitProcess@4 : PROC
extern __write : PROC
extern __read  : PROC; (dwa znaki podkre�lenia)
public  _main
.data
magazyn			db	80 dup (?)

nowa_linia		db	10

liczba_znakow	dd	?

index			db	?

znak			db	?

dwa				dd	2 dup (?)

jeden			dd	?

blad_tekst		db	'-1'
.code

_main PROC
; czytanie wiersza z klawiatury
push    80 ; maksymalna liczba znak�w
push    OFFSET magazyn
push    0  ; nr urz�dzenia (tu: klawiatura -nr 0)
call    __read ; czytanie znak�w z klawiatury
add     esp, 12	; usuniecie parametr�w ze stosu

; kody ASCII napisanego tekstu zosta�y wprowadzone do obszaru 'magazyn'
; funkcja read wpisuje do rejestru EAX liczb� wprowadzonych znak�w
mov     liczba_znakow, eax

; czytanie indexu z klawiatury
push    2 ; maksymalna liczba znak�w
push    OFFSET index
push    0  ; nr urz�dzenia (tu: klawiatura -nr 0)
call    __read ; czytanie znak�w z klawiatury
add     esp, 12	; usuniecie parametr�w ze stosu

mov		al,index
sub		al,'0'		;robimy liczbe z ascii
mov		index,al

; czytanie znak z klawiatury
push    2 ; maksymalna liczba znak�w
push    OFFSET znak
push    0  ; nr urz�dzenia (tu: klawiatura -nr 0)
call    __read ; czytanie znak�w z klawiatury
add     esp, 12	; usuniecie parametr�w ze stosu

; rejestr ECX pe�ni rol� licznika obieg�w p�tli
mov     ecx, liczba_znakow
mov     ebx, 0	; indeks pocz�tkowy
mov		edi, 0	;	liczenie ilosci wystapien
ptl:
inc		bl
cmp		bl,index		;sprawdzenie indexu od ktorego liczymy a ktory jest
dec		bl
jb		dalej				
;jesli juz powyzej indexu wyszukiwania
mov		edx,0
mov     dl, magazyn[ebx] ; pobranie kolejnego znaku	
cmp		dl, znak		;porownanie ze znakiem wybranym
jne		dalej			;jesli to nie ten to dalej
inc		edi				;jesli to ten to dodaj
dalej:
inc     ebx		; inkrementacja indeksu
loop	ptl     ; sterowanie p�tl�

;gotowa ilosc znakow
cmp		edi,0
je		blad
cmp		edi,9
ja		zamien
add		edi,'0'
mov		jeden,edi
push	1
push	OFFSET jeden
push	1
call	__write
add		esp,12

; wy�wietlenie przekszta�conego tekstu
koniec:
push	0
call    _ExitProcess@4   ; zako�czenie programu

zamien:
mov		edx,0
mov		eax,edi
mov		ebx,10
div		ebx			;w eax jest calosc, w edx jest reszta
add		eax,'0'
add		edx,'0'
mov		ebx,OFFSET dwa
mov		[ebx],eax
add		ebx,1
mov		[ebx],edx
push	2
push	OFFSET dwa
push	1
call	__write
add		esp,12
jmp		koniec

blad:
push	2
push	OFFSET blad_tekst
push	1
call	__write
add		esp,12
jmp		koniec

_main ENDP
END